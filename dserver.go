package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"

	"log"
	"net/http"
)

//get handler
func Search(w http.ResponseWriter, r *http.Request) {

	//checking header
	fmt.Println(w.Header())
	fmt.Println("started")
	fmt.Println("Post is chosen")
	fmt.Println(r.Header.Get("Origin"))
	allowedHeaders := "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token"
	w.Header().Set("Access-Control-Allow-Origin", "bill2-zendrulat.c9users.io")
	w.Header().Set("Access-Control-Allow-Origin", "https://preview.c9users.io")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", allowedHeaders)
	w.Header().Set("Access-Control-Expose-Headers", "Authorization")
	w.WriteHeader(http.StatusOK)

	switch r.Method {

	case "GET":
		fmt.Println(r.Header.Get("Origin"))

		db, err := sql.Open("mysql", "zendrulat:@/c9")
		if err != nil {
			log.Fatal(err)
		}
		defer db.Close()
		err = db.Ping()
		if err != nil {
			log.Fatal(err)
		}
		var id string
		var Serial string
		var Name string
		var Material string
		var Design string
		var Type string
		var ColorFinish string
		var ProductFit string
		var QuantitySold string
		var AttachmentStyle string
		var Logo string
		var PartNumber string
		var Notes string
		var Image string
		var Make string
		var Year string
		//rows, err := db.Query("SELECT * FROM c9.bill WHERE Design LIKE '3L%';")
		rows, err := db.Query("SELECT * FROM c9.bill")
		if err != nil {
			log.Fatal(err)
		}

		for rows.Next() {

			err = rows.Scan(&id, &Serial, &Name, &Material, &Design, &Type, &ColorFinish, &ProductFit, &QuantitySold, &AttachmentStyle, &Logo, &PartNumber, &Notes, &Image, &Make, &Year)
			if err != nil {
				log.Fatal(err)
			}

			fmt.Println("this is the link ", Name)
			fmt.Println("this is the meta ", Material)
			fmt.Println("this is the id ", id)

			part = append(part, Part{Id: id, Serial: Serial, Name: Name, Material: Material, Design: Design, Type: Type, ColorFinish: ColorFinish, ProductFit: ProductFit, QuantitySold: QuantitySold, AttachmentStyle: AttachmentStyle, Logo: Logo, PartNumber: PartNumber, Notes: Notes, Image: Image, Make: Make, Year: Year})
		}

		rows.Close()

		json.NewEncoder(w).Encode(part)
		part = nil

	default:
		fmt.Fprintf(w, "Sorry, only GET and POST methods are supported.")

	}
}
